var topicCount;
var studentCount;
var preferenceCount;
var matrix;

var defaultPreferenceCount = 3;
var defaultTopicCount = 10; //also serves as the default number of students

var goodInputs;

//after the page has loaded
document.addEventListener('DOMContentLoaded', function() {

  //retrieve the number of preferences
  if(localStorage.getItem('preferenceCount'))
    preferenceCount = localStorage.getItem('preferenceCount');
  else
    preferenceCount = defaultPreferenceCount;
  //reflect the retrieved value in the preferenceCount input field
  document.getElementById('PreferenceCountField').value = preferenceCount;

    //retrieve the number of topics
  if(localStorage.getItem('topicCount'))
    topicCount = localStorage.getItem('topicCount');
  else
    topicCount = defaultTopicCount;
  //reflect the retrieved value in the preferenceCount input field
  document.getElementById('TopicCountField').value = topicCount;

  //retrieve the number of students
  if(localStorage.getItem('studentCount'))
    studentCount = localStorage.getItem('studentCount');
  else
    studentCount = defaultTopicCount;

  //event handler for the inputCSV button
  $("#CSVImport").change(handleFileSelect);
  
  
  generateTable();
  
});

function updateParameters() {
  //retrieve the number of topics
  topicCount = parseInt(document.getElementById('TopicCountField').value, 10);
  localStorage.setItem('topicCount', topicCount);

  //TODO remove prefs in LocalStorage when we reduce the number of prefs
  
  //retrieve the number of preferences 
  preferenceCount = parseInt(document.getElementById('PreferenceCountField').value, 10);

  //check that preferenceCount doesn't exceed topicCount
  if(preferenceCount > topicCount) {
    preferenceCount = topicCount;
    document.getElementById('PreferenceCountField').value = topicCount;
  }
  
  localStorage.setItem('preferenceCount', preferenceCount);

  generateTable();
}
function addRow() {
  ++studentCount;
  console.log(studentCount);
  localStorage.setItem('studentCount', studentCount);

  generateTable();
}
function generateTable() {

  console.log("generating table with "+preferenceCount+" preferences and "+topicCount+" topics");

  goodInputs = new Map();
  
  //initialize the Map with falses
  for(var i = 0; i < studentCount; ++i) {
    for(j = 0; j < preferenceCount; ++j) {
      goodInputs.set(i+'-'+j, false);
    }
  }

  //reset table
  $("#PreferenceTable tr").remove();
  $("#PreferenceTableHead").attr("colspan", preferenceCount);
  
  // set up table
  var table = document.getElementById('PreferenceTable');
  
  for (var i = 0; i < studentCount; ++i) {
    
    var tr = document.createElement('tr');

    //create the remove row cross
    var removeRowCell = document.createElement('td');
    var removeRowButton = document.createElement('button');
    removeRowButton.setAttribute('type', 'button');
    removeRowButton.setAttribute('id', i+'RemoveRow');
    removeRowButton.className = 'close mr-1';
    removeRowButton.innerHTML = '&times;';
    removeRowButton.onclick = removeRow;

    removeRowCell.appendChild(removeRowButton);
    tr.appendChild(removeRowCell);

    //create a student label field
    var studentLabelCell = document.createElement('td');
    var studentLabelInput = document.createElement('input');
    studentLabelInput.setAttribute('type', 'text');
    studentLabelInput.setAttribute('id', 'StudentLabel'+i);
    studentLabelInput.className = 'label mr-3';
    studentLabelInput.setAttribute('placeholder', 'Student '.concat(i+1));
    studentLabelInput.onkeyup = saveValue;
    studentLabelInput.value = getSavedValue('StudentLabel'+i); //retrieve stored value
    studentLabelCell.appendChild(studentLabelInput);
    tr.appendChild(studentLabelCell);
    
    for (var j = 0; j < preferenceCount; ++j) {
      var td = document.createElement('td');
      td.setAttribute('id', 'tab-' + i + '-' + j);
      var input = document.createElement('input');
      input.setAttribute('type', 'text');
      input.setAttribute('id', i+'-'+j);
      input.value = getSavedValue(i+'-'+j); //retrieve the value in cache
      
      input.oninput = testInputHandler;
      input.onkeyup = saveValue;
      
      td.appendChild(input);
      tr.appendChild(td);
    }
    
    //add an extra cell to contain the result
    var resultCell = document.createElement('td');
    resultCell.setAttribute('id', 'result'+i);
    resultCell.className = 'pl-3';
    resultCell.innerHTML = getSavedValue('result'+i);
    tr.appendChild(resultCell);

    table.appendChild(tr);
  }
  //add extra empty row to the table for the + "add row" function
  var tr = document.createElement('tr');
  
  var addRowCell = document.createElement('td');
  var addRowButton = document.createElement('button');
  addRowButton.setAttribute('type', 'button');
  addRowButton.setAttribute('id', 'AddRowButton');
  addRowButton.className = 'close mr-1';
  addRowButton.innerHTML = '+';
  addRowButton.onclick = addRow;

  addRowCell.appendChild(addRowButton);
  tr.appendChild(addRowCell);

  table.appendChild(tr);

  //test the entire table after generating it
  testEntireTable();
}

function update() {

  //initialize the corresponding matrix with very high values
  matrix = [];
  for (var i = 0; i < studentCount; ++i) {
    matrix[i] = [];
    for (var j = 0; j < topicCount; ++j)
      matrix[i][j] = 100000;
  }

  //create an array of indexes to shuffle for randomness
  var indexes = [];
  for (var i = 0; i < studentCount; ++i)
    indexes[i] = i;
  //shuffle it
  for (let i = indexes.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [indexes[i], indexes[j]] = [indexes[j], indexes[i]];
  }

  //insert the values into the back-end matrix, w.r.t the shuffle indexes
  for (var i = 0; i < studentCount; ++i) {
    for (var j = 0; j < preferenceCount; ++j) {
      
      var topicInserted = parseInt(document.getElementById('tab-'+ indexes[i] + '-' + j).children[0].value);
      //check that this entry in the backend matrix is still un-initialized (== 100000), we don't want to override a cost in this matrix
      //this is to account for when a given topic is entered several times in the preferences' list.
      if(matrix[i][topicInserted-1] == 100000)
        matrix[i][topicInserted-1] = j+1;
    }
  }

  var m = new Munkres();
  var indices = m.compute(matrix);



  //clear the assigned topic column before writing them
  for(let i = 0; i < studentCount; ++i)
    document.getElementById('result'+i).innerHTML = "";

  //handle the results
  for(let i = 0; i < studentCount; ++i) {
    //display the results in the right-most result cell
    document.getElementById('result'+indexes[indices[i][0]]).innerHTML = indices[i][1]+1;
    //save the results to the cache
    localStorage.setItem('result'+indexes[indices[i][0]], indices[i][1]+1);
  }
}

function testInputHandler(e) {
  testInput(e.target.value, e.target.id);
}
//updates the goodValues Map and the input fields class given a value and its corresponding inputId
function testInput(preferenceValue, inputId) {
  //test the content of the preference input field
  if(parseInt(preferenceValue) > 0 && parseInt(preferenceValue) <= topicCount || preferenceValue == "") {
    document.getElementById(inputId).className = '';
    goodInputs.set(inputId, true);
  } else {
    document.getElementById(inputId).className = 'invalid';
    goodInputs.set(inputId, false);
  }
  checkGoodInputs();
}
//run the testInput function on all the preferences fields.
function testEntireTable() {
  for(var i = 0; i < studentCount; ++i) {
    for(var j= 0; j < preferenceCount; ++j) {
      var currentId = i+'-'+j;
      var currentPreference = document.getElementById(currentId).value;
      testInput(currentPreference,currentId);
    }
  }
}
//just check the state of the Map goodInputs and enable (or not) the compute button accordingly
function checkGoodInputs() {
  var success = true;
  
  for (var value of goodInputs.values()) {
    if(!value)
      success = false;
  }
  
  if(success) {
     //enable the compute button
    document.getElementById('ComputeButton').classList.remove('btn-secondary');
    document.getElementById('ComputeButton').classList.add('btn-success');
    document.getElementById('ComputeButton').disabled = false;
  } else {
     //disable the compute button
    document.getElementById('ComputeButton').classList.add('btn-secondary');
    document.getElementById('ComputeButton').classList.remove('btn-success');
    document.getElementById('ComputeButton').disabled = true;
  }
}
//Save the value function - save it to localStorage as (ID, VALUE)
function saveValue(e) {
  var id = e.target.id;  // get the sender's id to save it . 
  var val = e.target.value; // get the value. 
  localStorage.setItem(id, val); // Every time user writing something, the localStorage's value will override . 
}

//get the saved value function - return the value of "v" from localStorage. 
function getSavedValue(v) {
  if (!localStorage.getItem(v)) {
    return "";// You can change this to your default value. 
  }
  return localStorage.getItem(v);
}

//Toggles the show/hide link text next to the Instructions
function hideShowToggle() {
  if($('#InstructionContainer').hasClass('show')) {
    $('#ToggleLink').text('show');
  }
  else {
    $('#ToggleLink').text('hide');
  }
}
//Removes a row in the preference table
function removeRow(e) {
  var rowCount = studentCount;
  var rowToRemoveIndex = parseInt(e.target.id);

  //#### Move the entered data of the lines past the current row one row up the table
  for(var i = 0; i < rowCount-rowToRemoveIndex-1; ++i) {
    
    //declare indexes we need
    var currentRowIndex = rowToRemoveIndex+i;
    var nextRowIndex = currentRowIndex+1;

    //move the student label up
    document.getElementById('StudentLabel'+currentRowIndex).value = document.getElementById('StudentLabel'+nextRowIndex).value;
    //save the moved value to cache because the oninput event is not triggered by the previous instruction
    localStorage.setItem('StudentLabel'+currentRowIndex, document.getElementById('StudentLabel'+nextRowIndex).value);
    for(var j = 0; j < preferenceCount; ++j) {
      //move the preference input up
      document.getElementById(currentRowIndex+'-'+j).value = document.getElementById(nextRowIndex+'-'+j).value;
      //save the moved value to cache because the oninput event is not triggered by the previous instruction
      localStorage.setItem(currentRowIndex+'-'+j, document.getElementById(nextRowIndex+'-'+j).value);
    }
  }
  
  //#### Remove the last row of the table
  var lastRowIndex = rowCount-1;
  document.getElementById(lastRowIndex+'RemoveRow').parentElement.parentElement.remove(); //its DOM element
  //its cache entries
  localStorage.removeItem('StudentLabel'+lastRowIndex); //the student label
  for(var i = 0; i < preferenceCount; ++i)
    localStorage.removeItem(lastRowIndex+'-'+i); //the associated preferences

  //#### Re-generate the table to trigger the input checks
  studentCount = rowCount-1;
  localStorage.setItem('studentCount', studentCount);
  
  generateTable();
  
}

//clears the table from all student labels and all entered preferences
function clearTable() {
  for(var i = 0; i < studentCount; ++i) {
    document.getElementById('StudentLabel'+i).value = "";
    localStorage.removeItem('StudentLabel'+i);
    document.getElementById('result'+i).innerHTML = "";
    localStorage.removeItem('result'+i);
    for(var j= 0;j < preferenceCount; ++j) {
      
      var currentId = i+'-'+j;
      document.getElementById(currentId).value = "";
      localStorage.removeItem(currentId);
      testInput(0,currentId);
    }
  }
}

function exportCSV() {
 
  var CSVArray = [];

  //add the header line
  CSVArray[0] = ['"student"'];
  for(let i = 0; i < preferenceCount; ++i)
    CSVArray[0].push('"pref'.concat(i+1).concat('"'));

  CSVArray[0].push('"assignment"');
  
  //append the "normal" lines
  for(let i = 0; i < studentCount; ++i) {
    CSVArray[i+1] = ['"'.concat(document.getElementById('StudentLabel'+i).value).concat('"')];
    for(let j = 0; j < preferenceCount; ++j) {
      CSVArray[i+1].push('"'.concat(document.getElementById(i+'-'+j).value.concat('"')));
    }
    CSVArray[i+1].push('"'.concat(document.getElementById('result'.concat(i)).innerHTML.concat('"')));
  }

  //create CSV data to export
  let csvContent = "data:text/csv;charset=utf-8,";
  CSVArray.forEach(function(rowArray){
    let row = rowArray.join(";");
    csvContent += row + "\r\n";
  });
  //remove the last unecessary carriage return
  csvContent = csvContent.substring(0, csvContent.length-2);
  
  var encodedUri = encodeURI(csvContent);
  var link = document.getElementById("ExportLink");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "assignment.csv");
  link.click();

}

//Handler for the CSV Import function
function handleFileSelect(evt) {
  var file = evt.target.files[0];
  Papa.parse(file, {
    header: true,
    dynamicTyping: true,
    complete: function(results) {
      preferenceCount = results.meta.fields.length-2;
      studentCount = results.data.length;
      //reflect the retrieved value in the preferenceCount input field
      document.getElementById('PreferenceCountField').value = preferenceCount;
      
      //temporary variable declaration for retrieving the topic count
      var tmpTopicCount = 0;

      //set all the item in local storage
      for(let i = 0; i < studentCount; ++i) {
        
        var studentLabel = results.data[i].student;
        var assignment = results.data[i].assignment;
        if(studentLabel == null)
          studentLabel = "";
        if(assignment == null)
          assignment = "";
        localStorage.setItem('StudentLabel'+i, studentLabel);
        localStorage.setItem('result'+i, assignment);
        
        for(let j = 0; j < preferenceCount; ++j) {
          var pref = arguments[0].data[i]['pref'.concat(j+1)];
          if(pref == null)
            pref = "";
          localStorage.setItem(i+'-'+j, pref);
          if(pref > tmpTopicCount)
            tmpTopicCount = pref;
        }
      }
      
      topicCount = tmpTopicCount;
      document.getElementById('TopicCountField').value = topicCount;
      
      generateTable();
    }
  });
}
