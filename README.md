# Hungarian algorithm

This is a Javascript implementation of the [Hungarian algorithm][]. The IENT seminars can make use of it in order to assign topics to the students depending on their preferences, in an optimized manner.

[Hungarian algorithm]: https://en.wikipedia.org/wiki/Hungarian_algorithm

## Usage

Just open `index.html`, the rest is pretty much self-explanatory.  
  
When using Chrome/Chromium, you need to add the `--allow-file-access-from-files` when launching the program's executable to allow  the browser to load local files as scripts.

Firefox 68+ users, you need to set `privacy.file_unique_origin` to `false`. You
can do this by visiting `about:config` from inside Firefox.

## Copyright
This is adapted from this repository

```
https://github.com/addaleax/munkres-js
```
